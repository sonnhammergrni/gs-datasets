# GeneSPIDER datasets #

These GeneSPIDER datasets where created with the [GeneSPIDER toolbox](https://bitbucket.org/sonnhammergrni/genespider) and are stored in the structure provided by the toolbox [datastruct](https://bitbucket.org/sonnhammergrni/datastruct) in .json format.

### What is this repository for? ###

* GeneSPIDER datasets can be used for network inference algorithms and compare results to a [gs-network](https://bitbucket.org/sonnhammergrni/gs-networks) related to each dataset.

### How do I get set up? ###

* How to get it:

    Fetch this repository with the command

        git clone git@bitbucket.org:sonnhammergrni/gs-datasets.git ~/src/gs_datasets

    or download from [here](https://bitbucket.org/sonnhammergrni/gs-datasets/downloads)

* How to use it with the GeneSPIDER toolbox [datastruct](https://bitbucket.org/sonnhammergrni/datastruct):

    1. If the repository is downloaded simply load the datasets in MATLAB with the command

            Data = datastruct.Dataset.load('path/to/file.json')

         where `path/to/file.json` should be echanged for an actual path to a dataset file.

    2. Any datasets can be fetched directly from the on-line repository with the command

            Data = datastruct.Dataset.fetch('name-of-dataset.json')

         where `name-of-dataset.json` is the actual name as found in this repository. A complete URL to the dataset can also be provided. The `master` version of the file is fetched, meaning that if anything changes in the file a different dataset will be fetched and loaded. If a specific version is desired than provide the full path to the raw file including the hash to that commit instead.

* Dependencies

    No strict dependencies, however the [GeneSPIDER toolbox](https://bitbucket.org/sonnhammergrni/genespider) is recommended.


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

[![Creative Commons
License](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

These `GeneSPIDER datasets` are licensed under a [Creative Commons Attribution
4.0 International License](http://creativecommons.org/licenses/by/4.0/).
